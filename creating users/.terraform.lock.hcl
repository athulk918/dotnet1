# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/azuread" {
  version     = "1.1.1"
  constraints = "1.1.1"
  hashes = [
    "h1:0j7R54RJsHCvdU/xWHsR139njKea1g3yXVILQSDZ5Hs=",
    "zh:3f18644190901ab9bf2324e0547175961d9c012df16838caf813b6eb696eb5f6",
    "zh:5375040bc18f8b5f5904886133244be655f7dce3bc06cab7016a8d427e5e1301",
    "zh:59776019d7ef3ae7727c36d8eec6da52b4496b7152a86f023fa4091d3f5e2834",
    "zh:80ea97821d0cb0f0bf6c95238d01753445c6cfb3f3028f97f016eac2ed939379",
    "zh:a212c3ee8267c6cb741ffb2c2c654110405aad8916e76ccc8195c5c0f3a9d28c",
    "zh:ad2878ed73be84d1119dbeb604d4f997cc9e33797b9c41e89b86c4f2e43dc59a",
    "zh:c8b64fdf6f28dc4e1b141105534cbb39d11c6a7cc33a09470bb663820d91217d",
    "zh:cc7c3ba3e1d18ace829e971d107106a09facb5fc0c658d42ab9dcd87add09a0c",
    "zh:cde9059c900898400e21d07253d86b2340f5dcdccf9f39c42186904f31533dea",
    "zh:eb96bf0ac5e6ae7d3d191163eeefd49dd8f6b853d8b9798faa225f251d5fc6c0",
  ]
}
