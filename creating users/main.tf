resource "azuread_user" "user" {
  user_principal_name = "sabari2k2201_gmail.com#EXT#@sabari2k2201gmail.onmicrosoft.com"
  display_name        = "Abina Ajith"
  mail_nickname       = "Abina"
  password            = "SecretP@sswd99!"
}

resource "azuread_group" "group" {
  name    = "MyTerraformGroup"
  members = [
    azuread_user.user.object_id,
    /* more users */
  ]
}