# resource group in Azure
resource "azurerm_resource_group" "rg" {
  name     = "terraform_test"
  location = "East US"
}

# storage account in Azure
resource "azurerm_storage_account" "storage" {
  name                     = "importstorage1
  " # replace with unique name
  resource_group_name      = "terraform_test"
  location                 = "East US"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}