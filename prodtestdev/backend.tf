#to store statefile in storage account created

terraform {
    backend "azurerm" {
        resource_group_name = "prod"
        storage_account_name = "prodstorageathul"
        container_name = "prod"
        key = "prod.tfstate"
    }
}