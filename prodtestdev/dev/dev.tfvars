#this is used to override the variables.tf file
#variables.tf is good for providing default value
#terraform automatically recognises.
#if using this the default section in variables.tf file can be removed
#here it will ask the user to enter the value

rg_name               = "dev"
rg_location           = "East US"
app_service_plan_name = "webapp-serviceplan"
app_service_name      = "terraform-webapp-test"



