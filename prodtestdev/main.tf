resource "azurerm_resource_group" "rg" {
  name     = var.rg_name
  location = var.rg_location
}


###### use below to run for dev environment#
#terraform plan -out  .\dev\dev.tfplan `
#>> -state .\dev\dev.tfstate `
#>> -var-file .\dev\dev.tfvars




###### use below to run for prod environment#
#terraform plan -out  .\prod\prod.tfplan `
#>> -state .\prod\prod.tfstate `
#>> -var-file .\prod\prod.tfvars