#to store statefile in storage account created

terraform {
    backend "azurerm" {
        resource_group_name = "tfstate"
        storage_account_name = "tfstatestorageathul"
        container_name = "tfstate"
        key = "terraform.tfstate"
    }
}