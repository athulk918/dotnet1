resource "azurerm_resource_group" "rg" {
  name     = var.rg_name[terraform.workspace]
  location = var.rg_location
}



#use below commands
#terraform workspace list   --- to list workspaces
#terraform workspace new dev  == to create and switch to new workspace
#terraform plan -out dev.tfplan