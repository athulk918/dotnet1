variable "rg_name" {
  type = map
  default = {
    dev = "dev-rg"
    prod = "prod-rg"
  }

}
variable "rg_location" {
  type = string
  default = "East US"

}




